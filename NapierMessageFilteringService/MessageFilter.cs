﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NapierMessageFilteringService
{
    class MessageFilter
    {
        private ArrayList blockList;
        private string blockListPath = @"BlockList.txt";

        public MessageFilter()
        {
            blockList = new ArrayList();
        }

        public void init()
        {
            foreach (string s in Regex.Split(System.IO.File.ReadAllText(blockListPath), "\r\n"))
            {
                blockList.Add(s);
            }
        }

        public bool checkSender(string s)
        {
            foreach (string blocked in blockList)
            {
                if (s == blocked)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
