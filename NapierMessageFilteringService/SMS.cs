﻿using System.Text.RegularExpressions;

namespace NapierMessageFilteringService
{
    public class SMS : Message
    {
        private Regex regexInterNumber = new Regex(@"\+(?:[0-9] ?){6,14}[0-9]", RegexOptions.IgnoreCase);
        private TextSpeakExpander txtExpander;
        private const int SMSLENGTH = 140;
        private const int INTERNUMLENGTH = 15;
        private const string approvedSMS = @"approved-sms.txt";

        /*
         * SMS()
         *
         * Constructor for SMS
         */
        public SMS(string header, string body)
        {
            // Set message variables
            this.messageID = header.Substring(0, 10);
            this.dateRecieved = header.Substring(10);
            this.messageText = body;
            this.sender = extractSender();
            this.maxMessageLength = SMSLENGTH;
            this.maxSenderLength = INTERNUMLENGTH;

            // Deal with SMS Specific data
            this.txtExpander = new TextSpeakExpander();
            this.messageText = this.txtExpander.expandAbbreviations(this.messageText);
        } // SMS

        /*
         * extractSender
         *
         * Gets the international telephone number from the SMS
         */
        protected override string extractSender()
        {
            Match match = this.regexInterNumber.Match(this.messageText);
            this.messageText = this.messageText.Replace(match.Groups[0].Value + System.Environment.NewLine, "");
            return match.Groups[0].Value;
        } // extractSender

        public override void writeToFile()
        {
            string output = "";

            output += "Message ID: " + this.getMessageID() + System.Environment.NewLine;
            output += "Date Received: " + this.getDateRecieved().Substring(0, 2)
                   + "-" + this.getDateRecieved().Substring(2, 2)
                   + "-" + this.getDateRecieved().Substring(4, 4) + System.Environment.NewLine;
            output += "Sender: " + this.getSender() + System.Environment.NewLine;
            output += "Message: " + this.getMessageText() + System.Environment.NewLine;
            output += System.Environment.NewLine;

            System.IO.StreamWriter file = new System.IO.StreamWriter(approvedSMS, true);
            file.WriteLine(output);
            file.Close();
        }

        public override string ToString()
        {
            string output = "";
            output += "Message ID: " + this.getMessageID() + System.Environment.NewLine;
            output += "Date Received: " + this.getDateRecieved().Substring(0, 2)
                   + "-" + this.getDateRecieved().Substring(2, 2)
                   + "-" + this.getDateRecieved().Substring(4, 4) + System.Environment.NewLine;
            output += "Sender: " + this.getSender() + System.Environment.NewLine;
            output += "Message: " + this.getMessageText() + System.Environment.NewLine;
            output += System.Environment.NewLine;
            return output;
        }

        ~SMS() { }
    } // class SMS
}