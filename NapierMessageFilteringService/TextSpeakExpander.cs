﻿using System.Collections;
using System.Collections.Generic;
using CsvHelper;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;

namespace NapierMessageFilteringService
{
    public class TextSpeakExpander
    {
        private Dictionary<string, string> abbreviationDiectionary = null;
        private string dictionaryPath = @"textwords.csv";

        /*
         * TextSpeakExpander()
         *
         * Constructor for TextSpeakExpander
         */
        public TextSpeakExpander()
        {
            abbreviationDiectionary = new Dictionary<string, string>();
            TextReader reader = File.OpenText(dictionaryPath);
            var csv = new CsvReader(reader);
            while (csv.Read())
            {
                string key = csv.GetField<string>(0);
                string value = csv.GetField<string>(1);
                abbreviationDiectionary.Add(key, value);
            }
        } // TextSpeakExpander

        public string expandAbbreviations(string messageText)
        {
            string tmpBody = messageText;
            var regexWords = new Regex(@"[a-zA-Z]{1,}");

            foreach (Match m in regexWords.Matches(tmpBody))
            {
                string str = m.Value;
                if (abbreviationDiectionary.ContainsKey(str))
                {
                    tmpBody = tmpBody.Replace(str, str + " <" + abbreviationDiectionary[str] + ">");
                }
            }

            return tmpBody;
        } // expandAbbreviations
    }
}