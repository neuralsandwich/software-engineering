﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NapierMessageFilteringService
{
    public class Email : Message
    {
        private Regex regexEmailAddr = new Regex(@"\b[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\b", RegexOptions.IgnoreCase);
        private URLQuarantiner urlQuarantiner;
        private const int EMAILLENGTH = 1028;
        private const int EMAILADDRLENGTH = 254;
        private const string approvedEmail = @"approved-emails.txt";
        private string subject;

        /*
         * Email
         *
         * Constructor for Email
         */
        public Email(string header, string body)
        {
            // Set message variables
            this.messageID = header.Substring(0, 10);
            this.dateRecieved = header.Substring(10);
            this.messageText = body;
            this.sender = extractSender();
            this.maxMessageLength = EMAILLENGTH;
            this.maxSenderLength = EMAILADDRLENGTH;

            // Deal with Email specific data
            this.subject = extractSubject();
            this.urlQuarantiner = new URLQuarantiner();
            this.messageText = this.urlQuarantiner.quarantineURLs(messageText);
        } // Email

        public Email() { }

        /*
         * extractSender()
         *
         * extracts the sender from messageText
         */
        protected override string extractSender()
        {
            Match match = this.regexEmailAddr.Match(this.messageText);
            this.messageText = this.messageText.Replace(match.Groups[0].Value + System.Environment.NewLine, "");
            return match.Groups[0].Value;
        } // extractSender

        /*
         * extractSubject()
         *
         * extracts the subject from messageText
         */
        private string extractSubject()
        {
            // Split Message text into lines
            string[] lines = messageText.Split('\n');
            // Return second line - Drop the newline character
            var tmpSubject = lines[0].Substring(0, lines[0].Length - 1);
            this.messageText = this.messageText.Replace(tmpSubject + System.Environment.NewLine, "");
            return tmpSubject;
        }

        public string getSubject()
        {
            return this.subject;
        }

        public override void writeToFile()
        {
            string output = "";

            output += "Message ID: " + this.getMessageID() + System.Environment.NewLine;
            output += "Date Received: " + this.getDateRecieved().Substring(0, 2)
                   + "-" + this.getDateRecieved().Substring(2, 2)
                   + "-" + this.getDateRecieved().Substring(4, 4) + System.Environment.NewLine;
            output += "Sender: " + this.getSender() + System.Environment.NewLine;
            output += "Subject: " + this.getSubject() + System.Environment.NewLine;
            output += "Message: " + this.getMessageText() + System.Environment.NewLine;
            output += System.Environment.NewLine;

            System.IO.StreamWriter file = new System.IO.StreamWriter(approvedEmail, true);
            file.WriteLine(output);
            file.Close();
        }

        public override string ToString()
        {
            string output = "";
            output += "Message ID: " + this.getMessageID() + System.Environment.NewLine;
            output += "Date Received: " + this.getDateRecieved().Substring(0, 2)
                   + "-" + this.getDateRecieved().Substring(2, 2)
                   + "-" + this.getDateRecieved().Substring(4, 4) + System.Environment.NewLine;
            output += "Sender: " + this.getSender() + System.Environment.NewLine;
            output += "Subject: " + this.getSubject() + System.Environment.NewLine;
            output += "Message: " + this.getMessageText() + System.Environment.NewLine;
            output += System.Environment.NewLine;
            return output;
        }

        ~Email() { }
    }
}