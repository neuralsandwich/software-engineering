﻿using System;
using System.Windows;
namespace NapierMessageFilteringService
{
    public class MessageFactory
    {
        public MessageFactory()
        {
        }

        public Message createMessage(string header, string body)
        {
            Message msg = null;

            if (!string.IsNullOrEmpty(header) || !string.IsNullOrEmpty(body))
            {
                switch (header.Substring(0, 1))
                {
                    case "E":
                        msg = new Email(header, body);
                        if (!msg.isValid())
                        {
                            MessageBox.Show("Email is not valid" + msg.getMessageID());
                            msg = null;
                        }
                        break;

                    case "S":
                        msg = new SMS(header, body);
                        if (!msg.isValid())
                        {
                            MessageBox.Show("SMS is not valid" + msg.getMessageID());
                            msg = null;
                        }
                        break;

                    case "T":
                        msg = new Tweet(header, body);
                        if (!msg.isValid())
                        {
                            MessageBox.Show("Tweet is not valid" + msg.getMessageID());
                            msg = null;
                        }
                        break;

                    default:
                        break;
                }
            }

            if (msg != null)
            {
                return msg;
            }
            else
            {
                return null;
                //throw new InvalidOperationException();
            }


        } // createMessage
    } // MessageFactory
}