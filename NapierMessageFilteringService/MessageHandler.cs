﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NapierMessageFilteringService
{
    public class MessageHandler
    {
        private MessageFactory messageFactory;
        private MessageFilter messageFilter;
        private Dictionary<String, Message> messages;
        private MessageLoader messageLoader;
        private string blockMessages = @"blocked-messages.txt";

        public MessageHandler()
        {
            messageFactory = new MessageFactory();
            messageFilter = new MessageFilter();
            messages = new Dictionary<string, Message>();
            messageLoader = new MessageLoader();
            messageFilter.init();
        } // MessageHandler

        public void loadMessages(string path)
        {
            RawMessage[] rMessages;
            rMessages = messageLoader.read(path);
            processMessages(rMessages);
        } // loadMessages

        public Message[] getAllMessages()
        {
            return messages.Values.ToArray<Message>();
        } // getAllMessages

        public Message getMessage(String msgID)
        {
            Message msg;
            messages.TryGetValue(msgID, out msg);
            return msg;
        } // getMessage

        private void processMessages(RawMessage[] rMsgs)
        {
            foreach (RawMessage rMsg in rMsgs)
            {
                Message nMsg = messageFactory.createMessage(rMsg.header, rMsg.body);
                if (nMsg != null)
                {
                    if (messageFilter.checkSender(nMsg.getSender()) && !messages.ContainsKey(nMsg.getMessageID()))
                    {
                        messages.Add(nMsg.getMessageID(), nMsg);
                        nMsg.writeToFile();
                    }
                }
                else
                {
                    addToBlockedMessages(nMsg);
                }
            }
        } // processMessages

        public void filterMessage(RawMessage r)
        {
            Message msg = messageFactory.createMessage(r.header, r.body);
            if (msg != null)
            {
                if (messageFilter.checkSender(msg.getSender()) && !messages.ContainsKey(msg.getMessageID()))
                {
                    messages.Add(msg.getMessageID(), msg);
                    msg.writeToFile();
                }
                else
                {
                    addToBlockedMessages(msg);
                }
            }

        } // filterMessage

        public void addToBlockedMessages(Message m)
        {
            string output = "";

            output += "Message ID: " + m.getMessageID() + System.Environment.NewLine;
            output += "Date Received: " + m.getDateRecieved().Substring(0, 2)
                   + "-" + m.getDateRecieved().Substring(2, 2)
                   + "-" + m.getDateRecieved().Substring(4, 4) + System.Environment.NewLine;
            output += "Sender: " + m.getSender() + System.Environment.NewLine;
            output += "Message: " + m.getMessageText() + System.Environment.NewLine;
            output += System.Environment.NewLine;

            System.IO.StreamWriter file = new System.IO.StreamWriter(blockMessages, true);
            file.WriteLine(output);
            file.Close();
        }

        internal int getMessageCount()
        {
            return messages.Keys.ToArray().Length;
        }
    }
}
