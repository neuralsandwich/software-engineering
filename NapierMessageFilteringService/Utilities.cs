﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NapierMessageFilteringService
{
    public struct RawMessage
    {
        public string header;
        public string body;
    }
}
