﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace NapierMessageFilteringService
{
    public class Tweet : Message
    {
        private Regex regexHashTag = new Regex(@"#(\w+)", RegexOptions.IgnoreCase);
        private Regex regexMention = new Regex(@"@(\w+)", RegexOptions.IgnoreCase);
        private const int TWEETLENGTH = 140;
        private const int TWITTERIDLENGTH = 16;
        private const string approvedTweets = @"approved-tweets.txt";
        private const string trendingList = @"trending-list.txt";
        private const string mentionsList = @"mentions.txt";

        private Dictionary<string, int> hashtags = new Dictionary<string, int>();
        private Dictionary<string, int> mentions = new Dictionary<string, int>();

        private TextSpeakExpander txtExpander;

        /*
         * Tweet()
         *
         * Constructor for Tweet
         */
        public Tweet(string header, string body)
        {
            if (header.Length == 18)
            {
                // Set message variables
                this.messageID = header.Substring(0, 10);
                this.dateRecieved = header.Substring(10);
            }
            if (body.Length > 0)
            {
                this.messageText = body;
                this.sender = extractSender();
                this.maxMessageLength = TWEETLENGTH;
                this.maxSenderLength = TWITTERIDLENGTH;
            }
            // Check Sender and message length are within limits
            // Check the message has a date
            if ((this.messageText.Length < maxMessageLength || this.sender.Length < maxSenderLength)
                || (this.messageID.Length > 0 || this.sender.Length > 0)
                || this.dateRecieved.Length == 8)
            {
                // Deal with Tweet Specific data
                this.txtExpander = new TextSpeakExpander();
                this.txtExpander.expandAbbreviations(this.messageText);
                detectMeta();
            }
        } // Tweet()

        /*
         * detectMeta()
         *
         * Wrapper method for dealing with metadata
         */
        private void detectMeta()
        {
            detectHashTags();
            detectMentions();
            //this.messageText = this.txtExpander.expandAbbreviations(this.messageText);
        } // detectMeta

        /*
         * detectMentions()
         *
         * Searchs messageText for mentions
         */
        private void detectMentions()
        {
            foreach (Match m in regexMention.Matches(this.messageText))
            {
                string mention = m.Value;
                if (this.mentions.ContainsKey(mention))
                {
                    this.mentions[mention]++;
                }
                else
                {
                    this.mentions.Add(mention, 1);
                }
            }
        } // detectMentions

        /*
         * detectHashtags()
         *
         * Searches messageText for hastags
         */
        private void detectHashTags()
        {
            foreach (Match m in regexHashTag.Matches(this.messageText))
            {
                string htag = m.Value;
                if (this.hashtags.ContainsKey(htag))
                {
                    this.hashtags[htag]++;
                }
                else
                {
                    this.hashtags.Add(htag, 1);
                }
            }
        } // detectHashTags

        /*
         * extractSender()
         *
         * Gets the authors twitterID from the tweet
         */
        protected override string extractSender()
        {
            Match match = this.regexMention.Match(this.messageText);
            this.messageText = this.messageText.Replace(match.Groups[0].Value + " ", "");
            return match.Groups[0].Value;
        } // extractSender

        public override void writeToFile()
        {
            string output = "";

            output += "Message ID: " + this.getMessageID() + System.Environment.NewLine;
            output += "Date Received: " + this.getDateRecieved().Substring(0, 2)
                   + "-" + this.getDateRecieved().Substring(2, 2)
                   + "-" + this.getDateRecieved().Substring(4, 4) + System.Environment.NewLine;
            output += "Sender: " + this.getSender() + System.Environment.NewLine;
            output += "Message: " + this.getMessageText() + System.Environment.NewLine;
            output += System.Environment.NewLine;

            System.IO.StreamWriter file = new System.IO.StreamWriter(approvedTweets, true);
            file.WriteLine(output);
            file.Close();

            output = "";
            foreach (string s in this.hashtags.Keys.ToArray())
            {
                output += s + " ";
            }
            output += System.Environment.NewLine;
            file = new System.IO.StreamWriter(trendingList, true);
            file.WriteLine(output);
            file.Close();


            output += "";
            foreach (string s in this.mentions.Keys.ToArray())
            {
                output += s + " ";
            }
            output += System.Environment.NewLine;
            file = new System.IO.StreamWriter(mentionsList, true);
            file.WriteLine(output);
            file.Close();
        }

        /*
         * getMentions
         * 
         * Returns a string array of all Mentions
         */
        public string[] getMentions()
        {
            return this.mentions.Keys.ToArray();
        } // getMentions

        /*
         * getHashtags
         * 
         * Returns a string array of all Mentions
         */
        public string[] getHashtags()
        {
            return this.hashtags.Keys.ToArray();
        }

        public override string ToString()
        {
            string output = "";
            output += "Message ID: " + this.getMessageID() + System.Environment.NewLine;
            output += "Date Received: " + this.getDateRecieved().Substring(0, 2)
                   + "-" + this.getDateRecieved().Substring(2, 2)
                   + "-" + this.getDateRecieved().Substring(4, 4) + System.Environment.NewLine;
            output += "Sender: " + this.getSender() + System.Environment.NewLine;
            output += "Message: " + this.getMessageText() + System.Environment.NewLine;
            output += "Hashtags: ";
            foreach (string s in this.hashtags.Keys.ToArray())
            {
                output += s + " ";
            }
            output += System.Environment.NewLine;
            output += "Mentions: ";
            foreach (string s in this.mentions.Keys.ToArray())
            {
                output += s + " ";
            }
            output += System.Environment.NewLine;
            output += System.Environment.NewLine;
            return output;
        }

        ~Tweet() { }
    }
}