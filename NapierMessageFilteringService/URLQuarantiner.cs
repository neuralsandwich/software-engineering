﻿using System.Text.RegularExpressions;

namespace NapierMessageFilteringService
{
    public class URLQuarantiner
    {
        private Regex regexURL;
        private string tmpBody;

        /*
         * URLQuarantiner
         *
         * Constructor for URLQuarantiner
         */
        public URLQuarantiner()
        {
            this.regexURL = new Regex(@"([a-zA-Z]){3,}(:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?\.-]*)*\/?");
        } // URLQuarantiner

        /*
         * quarantineURLs
         *
         * Finds and replaces URLS with <URL Quarantined>
         */
        public string quarantineURLs(string messageText)
        {
            tmpBody = messageText;
            foreach (Match m in regexURL.Matches(tmpBody))
            {
                string str = m.Value;
                tmpBody = tmpBody.Replace(str, "<URL Quarantined>");
            }
            return tmpBody;
        } // quarantineURLs
    }
}