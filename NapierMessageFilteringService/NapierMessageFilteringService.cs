﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace NapierMessageFilteringService
{
    public class NMFS
    {
        MessageHandler messageHander = new MessageHandler();
        string path;

        public NMFS()
        {
            path = @"ReceivedMessages.txt";
        } // NMFS

        public void parseMessage(String s)
        {
            RawMessage rMsg = new RawMessage();
            String[] lines = Regex.Split(s, System.Environment.NewLine);

            rMsg.header = lines[0];
            for (int i = 1; i < lines.Length - 1; i++)
            {
                rMsg.body += (lines[i] + System.Environment.NewLine);
            }
            rMsg.body += lines[lines.Length - 1];

            messageHander.filterMessage(rMsg);
        } // parseMessage

        public void loadMessages()
        {
            messageHander.loadMessages(path);
        } // loadMessages

        public string printAllMessages()
        {
            String output = "";
            Message[] messages = messageHander.getAllMessages();
            foreach (Message msg in messages)
            {
                output += msg.ToString();
            }

            return output;
        } // printAllMessages

        public int getMessageCount()
        {
            return messageHander.getMessageCount();
        }
    }
}