﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NapierMessageFilteringService
{
    public class MessageLoader
    {

        public MessageLoader()
        {

        }

        public RawMessage[] read(string path)
        {
            RawMessage[] rMsgs = null;
            String[] allMsgs = null;
            // Split all messages on double newline
            allMsgs = Regex.Split(System.IO.File.ReadAllText(path), "\r\n\r\n");

            int i = 0;
            rMsgs = new RawMessage[allMsgs.Length];
            foreach (String s in allMsgs)
            {
                String[] lines = Regex.Split(allMsgs[i], System.Environment.NewLine);
                rMsgs[i].header = lines[0];
                for (int j = 1; j < lines.Length - 1; j++)
                {
                    rMsgs[i].body += lines[j] + System.Environment.NewLine;
                }
                rMsgs[i].body += lines[lines.Length - 1];
                i++;
            }

            return rMsgs;
        }
    }
}
