﻿using System.IO;

namespace NapierMessageFilteringService
{
    public abstract class Message
    {
        public int headerLength = 18;
        protected string dateRecieved;
        protected string messageID;
        protected string messageText;
        protected string sender;
        protected int maxMessageLength;
        protected int maxSenderLength;

        public Message() { }

        /*
         * Message()
         *
         * Public constructer for Message
         */
        public Message(string messageID, string dateRecieved, string sender, string messageText)
        {
            this.dateRecieved = dateRecieved;
            this.messageID = messageID;
            this.sender = sender;
            this.messageText = messageText;
        } // Message

        /*
         * validate()
         *
         * Validate message
         */
        public bool isValid()
        {
            // Check ID, Date Recieved conform with specification
            // Check sender and message text are within limits
            bool a = string.IsNullOrEmpty(this.dateRecieved) || string.IsNullOrEmpty(this.messageID) ||
                string.IsNullOrEmpty(this.sender) || string.IsNullOrEmpty(this.messageText);
            if (a)
            {
                return false;
            }
            bool b = (this.messageID.Length == 10) && (this.dateRecieved.Length == 8)
                && (this.sender.Length > 0) && (this.sender.Length < this.maxSenderLength)
                && (this.messageText.Length > 0) && (this.messageText.Length < maxMessageLength);
            if (b)
            {
                return true;
            }

            return false;
        } // validate

        public abstract void writeToFile();

        /*
         * extractSender()
         *
         * placeholder for child classes
         */
        protected abstract string extractSender();

        /*
         * getDateRecieved()
         *
         * Getter for dateRecieved
         */
        public string getDateRecieved()
        {
            return this.dateRecieved;
        } // getDateRecieved

        /*
         * getMessageID()
         *
         * Getter for MessageID
         */
        public string getMessageID()
        {
            return this.messageID;
        } // getMessageID

        /*
         * getMessageText()
         *
         * Getter for messageText
         */
        public string getMessageText()
        {
            return this.messageText;
        } // getMessageText

        /*
         * getSender()
         *
         * Getter for sender
         */
        public string getSender()
        {
            return this.sender;
        } // getSender

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Message m = (Message)obj;
            return (dateRecieved == m.dateRecieved) && (messageID == m.messageID)
                   && (messageText == m.messageText) && (sender == m.sender);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        ~Message()
        {
        }
    }
}