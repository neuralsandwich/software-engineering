﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NapierMessageFilteringService;

namespace NMFS_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NMFS nmfs;
        ApprovedMessages approvedWindow;

        public MainWindow()
        {
            nmfs = new NMFS();
            InitializeComponent();
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            nmfs.loadMessages();
            NMFSMessages.Text = nmfs.printAllMessages();
            messageCount.Content = "Message Count: " + nmfs.getMessageCount();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            nmfs.parseMessage(inputbox.Text);
            inputbox.Text = "Enter Message Here";
            NMFSMessages.Text = nmfs.printAllMessages();
            messageCount.Content = "Message Count: " + nmfs.getMessageCount();
        }

        private void inputbox_Focus(object sender, RoutedEventArgs e)
        {
            if (inputbox.Text == "Enter Message Here")
            {
                inputbox.Text = string.Empty;
            }
        } // inputbox_Focus

        private void inputbox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (inputbox.Text == string.Empty)
            {
                inputbox.Text = "Enter Message Here";
            }
        }

        private void Tweets_Click(object sender, RoutedEventArgs e)
        {
            approvedWindow = new ApprovedMessages();
            approvedWindow.setTextBoxContent(System.IO.File.ReadAllText(@"approved-tweets.txt"));
            approvedWindow.Show();

        }

        private void SMS_Click(object sender, RoutedEventArgs e)
        {
            approvedWindow = new ApprovedMessages();
            approvedWindow.setTextBoxContent(System.IO.File.ReadAllText(@"approved-sms.txt"));
            approvedWindow.Show();
        }

        private void Emails_Click(object sender, RoutedEventArgs e)
        {
            approvedWindow = new ApprovedMessages();
            approvedWindow.setTextBoxContent(System.IO.File.ReadAllText(@"approved-emails.txt"));
            approvedWindow.Show();
        }

        private void Blocked_Click(object sender, RoutedEventArgs e)
        {
            approvedWindow = new ApprovedMessages();
            approvedWindow.setTextBoxContent(System.IO.File.ReadAllText(@"blocked-messages.txt"));
            approvedWindow.Show();
        } // inputbox_LostFocus
    }
}
