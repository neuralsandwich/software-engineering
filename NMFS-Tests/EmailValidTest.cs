﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierMessageFilteringService;
using System;

namespace NMFS_Tests
{
    [TestClass]
    public class EmailValidTest
    {
        [TestMethod]
        public void TestValidMessageID()
        {
            string header = "E01234567805112013";
            string body = "john.smith@example.com" + System.Environment.NewLine
                           + "Test Email" + System.Environment.NewLine
                           + "Testing, Testing, 123";
            string expected = "E012345678";

            Email actualEmail = new Email(header, body);

            Assert.AreEqual(expected, actualEmail.getMessageID());
        } // TestValidMessageID

        [TestMethod]
        public void TestValidDateReceived()
        {
            string header = "E01234567805112013";
            string body = "john.smith@example.com" + System.Environment.NewLine
                           + "Test Email" + System.Environment.NewLine
                           + "Testing, Testing, 123";
            string expected = "05112013";

            Email actualEmail = new Email(header, body);

            Assert.AreEqual(expected, actualEmail.getDateRecieved());
        } // TestValidDateReceived

        [TestMethod]
        public void TestValidSender()
        {
            string header = "E01234567805112013";
            string body = "john.smith@example.com" + System.Environment.NewLine
                           + "Test Email" + System.Environment.NewLine
                           + "Testing, Testing, 123";
            string expected = "john.smith@example.com";

            Email actualEmail = new Email(header, body);

            Assert.AreEqual(expected, actualEmail.getSender());
        } // TestValidSender

        [TestMethod]
        public void TestValidSubject()
        {
            string header = "E01234567805112013";
            string body = "john.smith@example.com" + System.Environment.NewLine
                           + "Test Email" + System.Environment.NewLine
                           + "Testing, Testing, 123";

            string expected = "Test Email";

            Email actualEmail = new Email(header, body);

            Assert.AreEqual(expected, actualEmail.getSubject());
        } // TestValidSender

        [TestMethod]
        public void TestValidMessageText()
        {
            string header = "E01234567805112013";
            string body = "john.smith@example.com" + System.Environment.NewLine
                           + "Test Email" + System.Environment.NewLine
                           + "Testing, Testing, 123";

            // Space at the end of this line is due to rebuilding of message text
            string expected = "Testing, Testing, 123";

            Email actualEmail = new Email(header, body);

            Assert.AreEqual(expected, actualEmail.getMessageText());
        } // TestValidMessageText

        [TestMethod]
        public void TestValidURLQuarantine()
        {
            string header = "E01234567805112013";
            string body = "john.smith@example.com" + System.Environment.NewLine
                           + "XKCD" + System.Environment.NewLine
                           + "Here is that link http://xkcd.com";

            // Space at the end of this line is due to rebuilding of message text
            string expected = "Here is that link <URL Quarantined>";

            Email actualEmail = new Email(header, body);

            Assert.AreEqual(expected, actualEmail.getMessageText());
        } // TestValidURLQuarantine
    }
}