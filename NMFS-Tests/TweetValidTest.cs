﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierMessageFilteringService;
using System;

namespace NMFS_Tests
{
    [TestClass]
    public class TweetValidTest
    {
        [TestMethod]
        public void TestValidMessageID()
        {
            string header = "T12345678905112013";
            string body = "@stephenfry Terrifically simple idea encouraging children in the developing world into school, see @marysmeals in action http://www.child31film.com #Child31";
            string expected = "T123456789";

            Tweet actual = new Tweet(header, body);
            Assert.AreEqual(expected, actual.getMessageID());
        } // TestValidMessageID

        [TestMethod]
        public void TestValidDateReceived()
        {
            string header = "T12345679005112013";
            string body = "@stephenfry I know my next airline of choice… (via @KathyLette) pic.twitter.com/09iFDyfqSh";
            string expected = "05112013";

            Tweet actual = new Tweet(header, body);

            Assert.AreEqual(expected, actual.getDateRecieved());
        } // TestValidDateReceived

        [TestMethod]
        public void TestValidSender()
        {
            string header = "T12345679105112013";
            string body = "@stephenfry The founder of Not the 9 o'clock News, Spitting Image, Blackadder, QI - the great John Lloyd with @littleatoms here http://www.littleatoms.com/johnlloyd.htm ";
            string expected = "@stephenfry";

            Tweet actual = new Tweet(header, body);

            Assert.AreEqual(expected, actual.getSender());
        } // TestValidSender

        [TestMethod]
        public void TestValidMessageText()
        {
            string header = "T12345679205112013";
            string body = "@stephenfry Academy Award Nominee HOW TO SURVIVE A PLAGUE is out in cinemas from 8th November @SurvivePlagueUK";

            // Space at the end of this line is due to rebuilding of message text
            string expected = "Academy Award Nominee HOW TO SURVIVE A PLAGUE is out in cinemas from 8th November @SurvivePlagueUK";

            Tweet actual = new Tweet(header, body);

            Assert.AreEqual(expected, actual.getMessageText());
        } // TestValidMessageText

        [TestMethod]
        public void TestValidMentions()
        {
            string header = "T12345679305112013";
            string body = "@stephenfry Come to a spectacular #SalisburyCarolscarol concert in aid of @HelpforHeroes & @Soldierscharity, book now!  http://bit.ly/1ayxwkK";

            // Space at the end of this line is due to rebuilding of message text
            string[] expected = new string[] { "@HelpforHeroes", "@Soldierscharity" };

            Tweet actual = new Tweet(header, body);

            CollectionAssert.AreEqual(expected, actual.getMentions());
        } // TestValidMentions

        [TestMethod]
        public void TestValidHastags()
        {
            string header = "T12345679305112013";
            string body = "@stephenfry Francis had his leg driven through a log & left for nearly 2yrs #psychosis #mentalhealth. Now free! @BasicNeedsIntl  http://bit.ly/9Ce5Cc";

            // Space at the end of this line is due to rebuilding of message text
            string[] expected = new string[] { "#psychosis", "#mentalhealth" };

            Tweet actual = new Tweet(header, body);

            CollectionAssert.AreEqual(expected, actual.getHashtags());
        } // TestValidHastags
    }
}