﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierMessageFilteringService;

namespace NMFS_Tests
{
    [TestClass]
    public class MessageLoaderTest
    {
        [TestMethod]
        public void TestMessageRead()
        {
            MessageLoader ml = new MessageLoader();
            RawMessage[] actuals = ml.read(@"test-messages.txt");
            RawMessage[] expected = new RawMessage[3];
            // Message 1
            expected[0].header = "S23456789005112013";
            expected[0].body = "+234523452346" + System.Environment.NewLine
                      + "Hey, how are you? Been busy?";
            // Message 2
            expected[1].header = "T12345679305112013";
            expected[1].body = "@stephenfry Francis had his leg driven through a log & left for nearly 2yrs #psychosis #mentalhealth. Now free! @BasicNeedsIntl  http://bit.ly/9Ce5Cc";
            // Message 3
            expected[2].header = "E01234567805112013";
            expected[2].body = "john.smith@example.com" + System.Environment.NewLine
                             + "Test Email" + System.Environment.NewLine
                             + "Testing, Testing, 123";

            CollectionAssert.AreEqual(expected, actuals);
        }
    }
}