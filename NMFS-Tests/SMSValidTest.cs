﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierMessageFilteringService;

namespace NMFS_Tests
{
    [TestClass]
    public class SMSValidTest
    {
        [TestMethod]
        public void TestValidMessageID()
        {
            string header = "S23456789005112013";
            string body = "+234523452346" + System.Environment.NewLine
                        + "Hey, how are you? Been busy?";
            string expected = "S234567890";

            SMS actual = new SMS(header, body);
            Assert.AreEqual(expected, actual.getMessageID());
        } // TestValidMessageID

        [TestMethod]
        public void TestValidDateReceived()
        {
            string header = "S23456789106112013";
            string body = @"+234523452345" + System.Environment.NewLine
                        + "Hey!, not bad. How about you?";
            string expected = "06112013";

            SMS actual = new SMS(header, body);

            Assert.AreEqual(expected, actual.getDateRecieved());
        } // TestValidDateReceived

        [TestMethod]
        public void TestValidSender()
        {
            string header = "S23456789210112013";
            string body = @"+234523452346" + System.Environment.NewLine
                        + "Good, good. Just busy doing coursework.";
            string expected = "+234523452346";

            SMS actual = new SMS(header, body);

            Assert.AreEqual(expected, actual.getSender());
        } // TestValidSender

        [TestMethod]
        public void TestValidMessageText()
        {
            string header = "S23456789305112013";
            string body = @"+234523452345" + System.Environment.NewLine
                        + "Same! Good fun though. Hope you do well!";

            // Space at the end of this line is due to rebuilding of message text
            string expected = "Same! Good fun though. Hope you do well!";

            SMS actual = new SMS(header, body);

            Assert.AreEqual(expected, actual.getMessageText());
        } // TestValidMessageText

        [TestMethod]
        public void TestValidTextExpansion()
        {
            string header = "S23456789405112013";
            string body = @"+234523452389" + System.Environment.NewLine
                        + "LOL you too!";

            // Space at the end of this line is due to rebuilding of message text
            string expected = "LOL <Laughing out loud> you too!";

            SMS actual = new SMS(header, body);

            Assert.AreEqual(expected, actual.getMessageText());
        } // TestValidMessageText
    }
}
