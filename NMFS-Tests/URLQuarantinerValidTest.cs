﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierMessageFilteringService;

namespace NMFS_Tests
{
    [TestClass]
    public class URLValidTest
    {
        [TestMethod]
        public void TestInlineURL()
        {
            URLQuarantiner uq = new URLQuarantiner();

            string str = "Here is a URL http://www.google.com, that is surrounded by text.";
            string expected = "Here is a URL <URL Quarantined>, that is surrounded by text.";

            string actual = uq.quarantineURLs(str);
            Assert.AreEqual(expected, actual);
        }
    }
}
