﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierMessageFilteringService;

namespace NMFS_Tests
{
    [TestClass]
    public class MessageFactoryTest
    {
        [TestMethod]
        public void TestFactoryTweet()
        {
            MessageFactory msgFactory = new MessageFactory();
            string header = "T12345679305112013";
            string body = "@stephenfry Francis had his leg driven through a log & left for nearly 2yrs #psychosis #mentalhealth. Now free! @BasicNeedsIntl  http://bit.ly/9Ce5Cc";

            Message msg = msgFactory.createMessage(header, body);

            Assert.IsInstanceOfType(msg, typeof(Tweet));
        }

        [TestMethod]
        public void TestFactoryEmail()
        {
            MessageFactory msgFactory = new MessageFactory();
            string header = "E01234567805112013";
            string body = "john.smith@example.com" + System.Environment.NewLine
                           + "XKCD" + System.Environment.NewLine
                           + "Here is that link http://xkcd.com";

            Message msg = msgFactory.createMessage(header, body);
            Assert.IsInstanceOfType(msg, typeof(Email));
        }

        [TestMethod]
        public void TestFactorySMS()
        {
            MessageFactory msgFactory = new MessageFactory();
            string header = "S23456789405112013";
            string body = @"+234523452389" + System.Environment.NewLine
                        + "LOL you too!";

            Message msg = msgFactory.createMessage(header, body);
            Assert.IsInstanceOfType(msg, typeof(SMS));
        }
    }
}
