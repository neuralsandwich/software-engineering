﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierMessageFilteringService;

namespace NMFS_Tests
{
    [TestClass]
    public class MessageHandlerTest
    {
        [TestMethod]
        public void LoadTweetTest()
        {
            MessageHandler mh = new MessageHandler();
            mh.loadMessages(@"test-messages.txt");

            Message expected = new Tweet("T12345679305112013", "@stephenfry Francis had his leg driven through a log & left for nearly 2yrs #psychosis #mentalhealth. Now free! @BasicNeedsIntl  http://bit.ly/9Ce5Cc");

            Message actual = mh.getMessage("T123456793");

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void LoadEmailTest()
        {
            MessageHandler mh = new MessageHandler();
            mh.loadMessages(@"test-messages.txt");

            Message expected = new Email("E01234567805112013", @"john.smith@example.com
Test Email
Testing, Testing, 123");

            Message actual = mh.getMessage("E012345678");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void LoadSMSTest()
        {
            MessageHandler mh = new MessageHandler();
            mh.loadMessages(@"test-messages.txt");

            Message expected = new SMS("S23456789005112013", @"+234523452346
Hey, how are you? Been busy?");

            Message actual = mh.getMessage("S234567890");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void getAllTest()
        {
            MessageHandler mh = new MessageHandler();
            mh.loadMessages(@"test-messages.txt");

            Message[] expected = new Message[3];
            // Create All Test Messages
            // Create SMS
            expected[0] = new SMS("S23456789005112013", @"+234523452346
Hey, how are you? Been busy?");
            // Create Tweet
            expected[1] = new Tweet("T12345679305112013", "@stephenfry Francis had his leg driven through a log & left for nearly 2yrs #psychosis #mentalhealth. Now free! @BasicNeedsIntl  http://bit.ly/9Ce5Cc");
            // Create Email
            expected[2] = new Email("E01234567805112013", @"john.smith@example.com
Test Email
Testing, Testing, 123");

            Message[] actual = mh.getAllMessages();
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
