﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NapierMessageFilteringService;

namespace NMFS_Tests
{
    [TestClass]
    public class TextSpeakExpanderValidTest
    {
        [TestMethod]
        public void TestTextSpeakExpansion()
        {
            TextSpeakExpander txtExpander = new TextSpeakExpander();
            string str = "LOL";
            string actual = txtExpander.expandAbbreviations(str);
            string expected = "LOL <Laughing out loud>";
            Assert.AreEqual(expected, actual);
        }
    }
}
